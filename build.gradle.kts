import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
	repositories {
		mavenCentral()
	}
	dependencies {
		classpath("org.jetbrains.kotlin:kotlin-noarg:1.8.0")
		classpath("org.jetbrains.kotlin:kotlin-allopen:1.8.0")
	}
}

plugins {
	id("org.springframework.boot") version "2.7.5"
	id("io.spring.dependency-management") version "1.0.15.RELEASE"
	kotlin("jvm") version "1.8.0"
	kotlin("plugin.spring") version "1.8.0"
	id("org.jetbrains.kotlin.plugin.noarg") version "1.8.0"
}

group = "jglgwdding"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
	mavenCentral()
}


apply { plugin("kotlin-jpa") }
apply { plugin("kotlin-spring") }

allOpen {
	annotation("javax.persistence.Entity")
	annotation("javax.persistence.Embeddable")
	annotation("javax.persistence.MappedSuperclass")
}



dependencies {

	implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.apache.commons:commons-lang3:3.12.0")
	implementation("com.github.doyaaaaaken:kotlin-csv-jvm:1.7.0")
	implementation("io.github.g0dkar:qrcode-kotlin:3.3.0")
	implementation("org.apache.pdfbox:pdfbox:2.0.27")
	implementation("javax.mail:mail:1.5.0-b01")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
	runtimeOnly("com.h2database:h2")
	runtimeOnly("org.postgresql:postgresql")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	// https://mvnrepository.com/artifact/org.hamcrest/hamcrest-all
	testImplementation("org.hamcrest:hamcrest-all:1.3")
}
tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
