package jglgwedding.api.utils

import org.apache.commons.lang3.RandomStringUtils

object IdGenerator {
    fun generateRandomId(len: Int) = RandomStringUtils.randomAlphabetic(len).uppercase();
}