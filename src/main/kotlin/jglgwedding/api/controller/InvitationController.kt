package jglgwedding.api.controller

import jglgwedding.api.dto.InvitationDTO
import jglgwedding.api.dto.InviteeResponseDTO
import jglgwedding.api.model.Invitation
import jglgwedding.api.service.InvitationService
import org.springframework.web.bind.annotation.*

@RestController
class InvitationController(
    private val invitationService: InvitationService
) {


    @GetMapping("/invitations/{externalId}")
    @CrossOrigin
    fun getInvitation(@PathVariable externalId: String): InvitationDTO {
        return invitationService.getInvitationDTO(externalId)
    }

    @PutMapping("/invitations/{externalId}")
    @CrossOrigin
    fun updateInvitation(
        @PathVariable externalId: String,
        @RequestBody responses: List<InviteeResponseDTO>
    ): InvitationDTO {
        invitationService.saveResponses(externalId, responses.toSet())
        return invitationService.getInvitationDTO(externalId)
    }


}