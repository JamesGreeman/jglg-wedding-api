package jglgwedding.api.model.repository

import jglgwedding.api.model.Invitation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InvitationRepository: JpaRepository<Invitation, Long> {

    fun findByExternalId(externalId: String): Invitation?

}