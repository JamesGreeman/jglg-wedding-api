package jglgwedding.api.model.repository

import jglgwedding.api.model.Invitation
import jglgwedding.api.model.Invitee
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface InviteeRepository: JpaRepository<Invitee, UUID>