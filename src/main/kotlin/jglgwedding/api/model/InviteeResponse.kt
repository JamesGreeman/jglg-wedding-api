package jglgwedding.api.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Entity
data class InviteeResponse(
    @Id
    val id: UUID = UUID.randomUUID(),
    @JsonIgnore
    @OneToOne
    val invitee: Invitee,
    var attendingCeremony: Boolean = false,
    var attendingReception: Boolean = false,
    var receptionMinibus: Boolean = false,
    var ceremonyMinibus: Boolean = false,
    @Embedded
    var plusOne: PlusOne? = null,
    var notes: String = "",
)