package jglgwedding.api.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

data class PlusOne(
    var name: String,
) {
    constructor(): this("")
}