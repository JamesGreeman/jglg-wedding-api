package jglgwedding.api.model

import jglgwedding.api.utils.IdGenerator
import javax.persistence.*

@Entity
data class Invitation(
    @Id
    @Column(name = "invitation_number")
    val inviteNumber: Long,
    @Column(unique = true)
    val externalId: String = IdGenerator.generateRandomId(8),
    val description: String,
    val address: String? = null,
    val emailAddresses: String? = null,
    @Enumerated(EnumType.STRING)
    val language: Language,
    val ceremony: Boolean,
    val reception: Boolean,

    @OneToMany(mappedBy = "invitation",  cascade = [CascadeType.ALL],
        orphanRemoval = true, fetch = FetchType.EAGER)
    @OrderBy("ord ASC")
    val invitees: MutableList<Invitee> = mutableListOf(),
)
