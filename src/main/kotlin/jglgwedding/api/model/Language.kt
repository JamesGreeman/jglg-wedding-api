package jglgwedding.api.model

enum class Language(val textName: String, val locale: String) {
    EN("English", "en-GB"),
    FR("French", "fr-FR")
}