package jglgwedding.api.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
data class Invitee(
    @Id
    val id: UUID = UUID.randomUUID(),
    val ord: Int,
    @JsonIgnore
    @ManyToOne
    val invitation: Invitation,
    val name: String,
    val hasPlusOne: Boolean,

    @OneToOne(mappedBy = "invitee", cascade = [CascadeType.ALL],
        orphanRemoval = true, fetch = FetchType.EAGER)
    var response: InviteeResponse? = null
) {
    override fun toString(): String {
        return "Invitee(id=$id, name='$name', hasPlusOne=$hasPlusOne)"
    }

    fun count(): Int {
        if (response == null) {
            return 0
        }
        if (response!!.plusOne == null) {
            return 1
        }
        return 2
    }
}