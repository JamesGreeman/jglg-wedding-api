package jglgwedding.api.service

import jglgwedding.api.dto.*
import jglgwedding.api.model.PlusOne
import jglgwedding.api.model.Invitation
import jglgwedding.api.model.Invitee
import jglgwedding.api.model.InviteeResponse
import jglgwedding.api.model.repository.InvitationRepository
import jglgwedding.api.model.repository.InviteeRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.util.StopWatch
import java.util.*
import javax.transaction.Transactional
import javax.xml.bind.JAXBElement.GlobalScope

@Service
class InvitationService(
    private val invitationRepository: InvitationRepository,
    private val inviteeRepository: InviteeRepository,
    private val emailSender: EmailSender,
) {

    @Value("\${mail.to.addresses}")
    private lateinit var toAddresses: String

    companion object {
        val logger = LoggerFactory.getLogger(InvitationService::class.java)
    }

    @Transactional
    fun getInvitationDTO(externalId: String): InvitationDTO {
            return getInvitation(externalId).toDTO()
    }

    private fun Invitation.toDTO() = InvitationDTO(
        externalId = externalId,
        inviteName = description,
        locale = language.locale,
        ceremony = ceremony,
        reception = reception,
        invitees = invitees.map { invitee ->
            InviteeDTO(
                id = invitee.id,
                name = invitee.name,
                eligibleForPlusOne = invitee.hasPlusOne,
                plusOne = invitee.response?.plusOne,
                attendingCeremony = invitee.response?.attendingCeremony ?: ceremony,
                attendingReception = invitee.response?.attendingReception ?: reception,
                minibusCeremony = invitee.response?.ceremonyMinibus ?: false,
                minibusReception = invitee.response?.receptionMinibus ?: false,
                notes = invitee.response?.notes ?: ""
            )
        }
    )

    fun getInvitation(externalId: String) : Invitation {
        val normalisedId = externalId.uppercase().trim()
        return invitationRepository.findByExternalId(normalisedId)
            ?: run {
                logger.error("Failed to find invite for '$normalisedId'")
                throw NoSuchElementException("No invitation with external id: $normalisedId")
            }
    }

    @Transactional
    fun saveResponses(externalId: String, responses: Set<InviteeResponseDTO>) {
        val normalisedId = externalId.uppercase().trim()
        val responsesWithInvitees = responses.map {
            val invitee = inviteeRepository.findById(it.id).orElseThrow {
                NoSuchElementException("No invitee with id ${it.id} association to invitation $normalisedId")
            }
            if (invitee.invitation.externalId != normalisedId) {
                logger.error("The invitee id specified does not belong to invitation '$normalisedId'")
                throw IllegalArgumentException("The invitee id specified does not belong to invitation $normalisedId")
            }
            it  to invitee
        }.toMap()


        emailResponse(responsesWithInvitees)

        responsesWithInvitees.forEach { (response, invitee) ->

            val inviteeResponse = invitee.response ?: InviteeResponse(invitee = invitee)

            inviteeResponse.attendingCeremony = invitee.invitation.ceremony && response.attendingCeremony
            inviteeResponse.attendingReception = invitee.invitation.reception && response.attendingReception
            inviteeResponse.ceremonyMinibus = invitee.invitation.ceremony && response.ceremonyMinibus ?: false
            inviteeResponse.receptionMinibus = invitee.invitation.reception && response.receptionMinibus ?: false
            inviteeResponse.notes = response.notes ?: ""
            inviteeResponse.plusOne = response.guest?.let {
                if (!invitee.hasPlusOne) {
                    throw IllegalArgumentException("Guest ${invitee.id} does not have a plus one")
                }
                PlusOne(
                    name = response.guest.name,
                )
            }
            inviteeResponse.ceremonyMinibus = response.ceremonyMinibus ?: false
            inviteeResponse.receptionMinibus = response.receptionMinibus ?: false
            invitee.response = inviteeResponse
        }
    }

    fun emailResponse(response: Map<InviteeResponseDTO, Invitee>) {

        val responseText = response.map { (response, invitee) ->

            """
                |<b>${invitee.name}</b>
                |${if (invitee.invitation.ceremony) {
                    """
                    |Ceremony: ${if (response.attendingCeremony) "ATTENDING" else "NOT ATTENDING"} and Minibus: ${if (response.ceremonyMinibus == true) "YES" else "NO"}
                    """.trimMargin()
                } else "" }
                |${if (invitee.invitation.reception) {
                """
                    |Reception: ${if (response.attendingReception) "ATTENDING" else "NOT ATTENDING"} and Minibus: ${if (response.receptionMinibus == true) "YES" else "NO"}
                    """.trimMargin()
                 } else "" 
                } ${if (invitee.hasPlusOne && response.guest != null) "\n|Is bring guest: ${response.guest.name}" else ""
                } ${if (response.notes.isNullOrBlank()) "" else "\n|Comments: ${response.notes}"}
            """.trimMargin()
        }

        val content = """
            |New RSVP Response received:
            |
            |${responseText.joinToString("\n\n")}
        """.trimMargin().replace("\n", "<br>\n")

        val subject = "New response from ${response.map { (_, invitee) -> invitee.name }.joinToString(" | " )}"

        emailSender.sendEmail(toAddresses, subject, content)
    }
}