package jglgwedding.api.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

@Service
class EmailSender {


    @Value("\${mail.username}")
    private lateinit var username: String
    @Value("\${mail.password}")
    private lateinit var password: String

    private val prop = Properties().also {
        it["mail.smtp.auth"] = true;
        it["mail.smtp.starttls.enable"] = "true";
        it["mail.smtp.host"] = "smtp.gmail.com";
        it["mail.smtp.port"] = "587";
        it["mail.smtp.ssl.trust"] = "smtp.gmail.com";
        it["mail.smtp.starttls.required"] = "true";
        it["mail.smtp.ssl.protocols"] = "TLSv1.2";
    }

    private val session = Session.getDefaultInstance(prop, object : Authenticator() {
        override fun getPasswordAuthentication() = PasswordAuthentication(username, password)
    })

    @Async
    fun sendEmail(to: String, subject: String, content: String) {
        val message: Message = MimeMessage(session)
        message.setFrom(InternetAddress(username))
        val toAddresses = to.split(";").map { InternetAddress.parse(it).first() }.toTypedArray()
        message.setRecipients(
            Message.RecipientType.TO, toAddresses
        )
        message.subject = subject

        val mimeBodyPart = MimeBodyPart()
        mimeBodyPart.setContent(content, "text/html; charset=utf-8")

        val multipart: Multipart = MimeMultipart()
        multipart.addBodyPart(mimeBodyPart)

        message.setContent(multipart)

        Transport.send(message)
    }

}