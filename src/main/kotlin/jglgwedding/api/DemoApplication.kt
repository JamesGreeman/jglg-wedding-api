package jglgwedding.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.PropertySource
import org.springframework.context.annotation.PropertySources

@SpringBootApplication(scanBasePackages = ["jglgwedding.api"])
@PropertySources(
	PropertySource("classpath:application.properties"),
	PropertySource("classpath:secure.properties")
)
class DemoApplication {

}

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args)
}
