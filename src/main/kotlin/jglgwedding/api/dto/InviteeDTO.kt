package jglgwedding.api.dto

import jglgwedding.api.model.PlusOne
import java.util.*

data class InviteeDTO(
    val id: UUID,
    val name: String,
    val eligibleForPlusOne: Boolean,
    val attendingCeremony: Boolean,
    val attendingReception: Boolean,
    val minibusCeremony: Boolean,
    val minibusReception: Boolean,
    val plusOne: PlusOne?,
    val notes: String
)
