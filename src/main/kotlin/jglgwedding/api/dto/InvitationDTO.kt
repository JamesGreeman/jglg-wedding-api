package jglgwedding.api.dto

data class InvitationDTO(
    val externalId: String,
    val inviteName: String,
    val locale: String,
    val ceremony: Boolean,
    val reception: Boolean,
    val invitees: List<InviteeDTO>,
)
