package jglgwedding.api.dto

import com.fasterxml.jackson.annotation.JsonInclude
import java.util.UUID

data class InviteeResponseDTO(
    val id: UUID,
    val guest : InviteeGuestDTO?,
    val attendingCeremony: Boolean,
    val attendingReception: Boolean,
    val ceremonyMinibus: Boolean? = null,
    val receptionMinibus: Boolean? = null,
    val notes: String? = null,
)

data class InviteeGuestDTO(
    val name: String,
)
