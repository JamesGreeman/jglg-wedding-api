package jglgwedding.api.bootstrap

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import io.github.g0dkar.qrcode.QRCode
import jglgwedding.api.model.Invitation
import jglgwedding.api.model.Invitee
import jglgwedding.api.model.Language
import jglgwedding.api.model.repository.InvitationRepository
import jglgwedding.api.service.EmailSender
import jglgwedding.api.utils.IdGenerator
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.PDPageContentStream
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.font.PDFont
import org.apache.pdfbox.pdmodel.font.PDType1Font
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File
import java.io.FileOutputStream
import java.util.*
import javax.annotation.PostConstruct
import javax.mail.*


@Service
class BootstrapService(
    @Autowired private val invitationRepository: InvitationRepository
) {
    companion object {

//        @JvmStatic
//        fun main(args: Array<String>) {
//            buildInvitations()
//        }

        fun buildInvitations() {

            val invitations = parseInvitations()

            invitations.forEach { println(it) }

            val (french, english) = invitations.partition { it.language == Language.FR }

            val frenchBoth = french.filter { it.ceremony && it.reception }
            val frenchCeremony = french.filter { it.ceremony && !it.reception }
            val frenchReception = french.filter { !it.ceremony && it.reception }
            val englishBoth = english.filter { it.ceremony && it.reception }
            val englishCeremony = english.filter { it.ceremony && !it.reception }
            val englishReception = english.filter { !it.ceremony && it.reception }

            fun List<Invitation>.getEmails() = this.flatMap { it.emailAddresses?.split(";") ?: emptyList() }
                .map { it.trim() }
                .joinToString(";")

            println("French Both Emails: ${frenchBoth.getEmails()}")
            println("French Ceremony Emails: ${frenchCeremony.getEmails()}")
            println("French Reception Emails: ${frenchReception.getEmails()}")
            println("English Both Emails: ${englishBoth.getEmails()}")
            println("English Ceremony Emails: ${englishCeremony.getEmails()}")
            println("English Reception Emails: ${englishReception.getEmails()}")

            val file = File("/tmp/InvitePDFs")
            val qrsFolder = File("/tmp/QRs")
            if (!file.exists()) file.mkdir()
            if (!qrsFolder.exists()) qrsFolder.mkdir()

            val document = PDDocument()

            invitations.forEach { invitation ->

                val names = invitation.invitees.map { it.name }
                val page = PDPage(PDRectangle.A6)

                fun PDPageContentStream.writeCentresAtYPos(
                    text: String,
                    yPos: Float,
                    font: PDFont = PDType1Font.HELVETICA,
                    size: Float = 12f,
                    colour: Triple<Float, Float, Float> = Triple(0f, 0f, 0f),
                ) {
                    val textWidth = font.getStringWidth(text) / 1000 * size
                    val xPos = (page.mediaBox.width - textWidth) / 2.0f
                    setFont(font, size)
                    beginText()
                    setNonStrokingColor(colour.first, colour.second, colour.third)
                    setStrokingColor(colour.first, colour.second, colour.third)
                    newLineAtOffset(xPos, yPos)
                    showText(text)
                    endText()
                }
                document.addPage(page)

                val contentStream = PDPageContentStream(document, page)

                names.forEachIndexed{ index, name ->
                    contentStream.writeCentresAtYPos(name, 380f -(6 * index), size = 6f)
                }
                contentStream.writeCentresAtYPos(if (invitation.language == Language.EN) "Please respond on our website:" else "Merci de répondre sur notre site", 325f)
                contentStream.writeCentresAtYPos("https://james-laurie.wedding", 310f, colour = Triple(0f,0f, 1.0f))
                contentStream.writeCentresAtYPos(if (invitation.language == Language.EN) "Alternatively email us at:" else "Vous pouvez également nous envoyer un e-mail à", 290f)
                contentStream.writeCentresAtYPos("jamie.greeman@gmail.com - lauriegely@hotmail.fr", 275f, colour = Triple(0f,0f, 1.0f))
                contentStream.writeCentresAtYPos("jglg.wedding@gmail.com", 260f, colour = Triple(0f,0f, 1.0f))

                val path = "$qrsFolder/${invitation.invitees.joinToString { it.name }}.png"
                FileOutputStream(path).use {
                    QRCode("https://james-laurie.wedding?inviteCode=${invitation.externalId}")
                        .render(cellSize = 3)
                        .writeImage(it)
                }
                val image: PDImageXObject = PDImageXObject.createFromFile(path, document)
                contentStream.drawImage(image, 100f, 140f)

                contentStream.writeCentresAtYPos("Your Invitation Code:", 120f)
                contentStream.writeCentresAtYPos(invitation.externalId, 105f, font = PDType1Font.COURIER_BOLD)
                contentStream.close()


            }
            document.save("${file.path}/Invites.pdf")
            document.close()

            invitations.forEach { invitation ->
                val nameString = invitation.invitees.first().name
                val formattedAddress = invitation.address?.replace(", ", "\n")

                println(nameString)
                println(formattedAddress)
                println()
                println()
                println()

            }

        }

        private fun parseInvitations(): List<Invitation> {
            val invitations =
                csvReader().open(BootstrapService::class.java.getResource("/guestList.csv")!!.openStream()) {
                    readAllWithHeaderAsSequence().mapIndexed { index, map ->
                        val familyName = map["Family Name"]!!
                        val firstNames = map["First Names"]!!.split(",")
                        val names = firstNames.map {
                            val trimmed = it.trim()
                            if (trimmed.contains(" ")) {
                                trimmed
                            } else {
                                "$trimmed $familyName"
                            }
                        }
                        val invitedTo = map["Invited To"]!!.trim()
                        val ceremony = invitedTo in listOf("Les deux", "Mariage mairie")
                        val reception = invitedTo in listOf("Les deux", "Mariage fête")
                        Invitation(
                            inviteNumber = index.toLong(),
                            externalId = map["Code"]!!,
                            description = familyName,
                            address = map["Address"]?.takeIf { it != "0" },
                            emailAddresses = map["Emails"].takeIf { it != "0" },
                            ceremony = ceremony,
                            reception = reception,
                            language = if (map["Language"]?.trim() == "French") Language.FR else Language.EN,
                        ).also { invitation ->
                            invitation.invitees.addAll(names.mapIndexed { index, name ->
                                Invitee(
                                    ord = index,
                                    name = name,
                                    hasPlusOne = map["Plus 1"]?.trim() == "YES",
                                    invitation = invitation
                                )
                            })
                        }

                    }.toList()
                }
            return invitations
        }
    }

    @PostConstruct
    fun afterStartup() {
        if (invitationRepository.count() == 0L) {
            saveToDatabase(parseInvitations())
        }
    }

    private fun saveToDatabase(invitations: List<Invitation>) {
        invitationRepository.saveAll(invitations)
        invitations.forEach { println(it.externalId) }

    }

}